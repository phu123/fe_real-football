import React, { lazy } from 'react'
import { Link, Router, Redirect } from '@reach/router'
import { Result } from 'antd'

const HomePage = lazy(() => import('./views/HomePage'))
const ProductDetailPage = lazy(() => import('./views/ProductDetailPage'))
const CartPage = lazy(() => import('./views/CartPage'))

const PageNotPound = () => (
  <Result
    status='404'
    title='404'
    subTitle='Sorry, the page you visited does not exist.'
    extra={<Link to='/'>Back Home</Link>}
  />
)

const Routes = () => (
  <Router style={{ backgroundColor: 'var(--secondary)' }}>
    <Redirect from='/' to='/trang-chu' />
    <HomePage path='/trang-chu' />

    <ProductDetailPage path='/san-pham/:id' />

    <CartPage path='/gio-hang' />
    <PageNotPound path='*' />
  </Router>
)

export default Routes

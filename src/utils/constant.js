const PRODUCT_STATUS = {
  0: 'Active',
}

const CATEGORY = {
  1: 'Organic',
}

module.exports = { PRODUCT_STATUS, CATEGORY }

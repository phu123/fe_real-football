import qs from 'qs'

import { buildRequest } from './index.js'

//----------- User API -----------

//----------- Product API -----------

export const getProducts = (filter) => {
  return buildRequest(`/product?${qs.stringify(filter)}`, {
    method: 'GET',
  }).request()
}

//----------- Product By Id API -----------
export const getProductById = (id) => {
  return buildRequest(`/product/detail/${id}`, {
    method: 'GET',
  }).request()
}

//----------- Blog API -----------

export const getBlogs = (filter) => {
  return buildRequest(`/blog?${qs.stringify(filter)}`, {
    method: 'GET',
  }).request()
}

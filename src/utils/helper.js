const calculateSalePrice = (discount, price) =>
  discount ? Math.round(price * (1 - discount)) : price

const avgRating = (rate) => {
  let avg = 0
  if (rate) {
    avg = Math.round(
      rate.reduce((prev, curr) => prev + curr.rating, 0) / rate.length
    )
  }
  return avg
}

module.exports = { calculateSalePrice, avgRating }

const createAction = (type, ...argNames) =>
  function cA(...args) {
    const action = { type }

    argNames.forEach((arg, index) => {
      action[argNames[index]] = args[index]
    })

    return action
  }

const createActionSet = actionName => ({
  REQUEST: `${actionName}_REQUEST`,
  SUCCESS: `${actionName}_SUCCESS`,
  FAILURE: `${actionName}_FAILURE`,
})

const list2IdObject = arr =>
  arr.reduce((acc, cur) => ({ ...acc, [cur.id]: cur }), {})

export { createAction, createActionSet, list2IdObject }

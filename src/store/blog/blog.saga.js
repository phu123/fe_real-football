import { takeLatest, call, put } from 'redux-saga/effects'
import { notification } from 'antd'
import { getBlogs } from 'utils/api/apiRouter'

import { types, actionCreator } from './blog.meta'

const errMessage = {
  message: 'Opp! Đã xảy ra lỗi',
  description: 'Liên hệ ban quản trị viên để biết thêm thông tin chi tiết!',
}

function* getBlogsMonitor(action) {
  try {
    const { body } = yield call(getBlogs, action.payload)
    yield put(actionCreator.getBlogsSuccess(body))
  } catch (error) {
    notification.error(errMessage)
  }
}

export default function* blogSaga() {
  yield takeLatest(types.GET_BLOGS_REQUEST, getBlogsMonitor)
}

import { handleActions } from 'redux-actions'
import { types } from './blog.meta'

const initialState = {
  items: {
    total: 0,
    list: [],
    bySlug: {},
  },
}

const getBlogsSuccess = (state, action) => ({
  ...state,
  items: {
    list: action.payload.data,
    total: action.payload.total,
  },
})

export default handleActions(
  {
    [types.GET_BLOGS_SUCCESS]: getBlogsSuccess,
  },
  initialState
)

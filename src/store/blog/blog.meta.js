import { createAction } from 'redux-actions'

export const types = {
  GET_BLOGS_REQUEST: 'GET_BLOGS_REQUEST',
  GET_BLOGS_SUCCESS: 'GET_BLOGS_SUCCESS',
}

export const actionCreator = {
  getBlogs: createAction(types.GET_BLOGS_REQUEST),
  getBlogsSuccess: createAction(types.GET_BLOGS_SUCCESS),
}

import { combineReducers } from 'redux'

import product from './product/product.reducer'
import cart from './cart/cart.reducer'
import blog from './blog/blog.reducer'

export default combineReducers({ product, cart, blog })

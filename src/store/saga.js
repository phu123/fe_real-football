import { all, fork } from 'redux-saga/effects'

import product from './product/product.saga'
import cart from './cart/cart.saga'
import blog from './blog/blog.saga'

export default function* mainSaga() {
  yield all([fork(product), fork(cart), fork(blog)])
}

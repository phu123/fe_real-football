import { createAction } from 'redux-actions'

export const types = {
  ADD_TO_CART_REQUEST: 'ADD_TO_CART_REQUEST',
  ADD_TO_CART_SUCCESS: 'ADD_TO_CART_SUCCESS',

  PLUS_QUANTITY_PRODUCT_REQUEST: 'PLUS_QUANTITY_PRODUCT_REQUEST',
  PLUS_QUANTITY_PRODUCT_SUCCESS: 'PLUS_QUANTITY_PRODUCT_SUCCESS',

  MINUS_QUANTITY_PRODUCT_REQUEST: 'MINUS_QUANTITY_PRODUCT_REQUEST',
  MINUS_QUANTITY_PRODUCT_SUCCESS: 'MINUS_QUANTITY_PRODUCT_SUCCESS',

  REMOVE_PRODUCT_FROM_CART_REQUEST: 'REMOVE_PRODUCT_FROM_CART_REQUEST',
  REMOVE_PRODUCT_FROM_CART_SUCCESS: 'REMOVE_PRODUCT_FROM_CART_SUCCESS',
}

export const actionCreator = {
  actAddProductToCart: createAction(types.ADD_TO_CART_REQUEST),
  actAddProductToCartSuccess: createAction(types.ADD_TO_CART_SUCCESS),

  actPlusQuantityProductToCart: createAction(
    types.PLUS_QUANTITY_PRODUCT_REQUEST
  ),
  actPlusQuantityProductToCartSuccess: createAction(
    types.PLUS_QUANTITY_PRODUCT_SUCCESS
  ),

  actMinusQuantityProductToCart: createAction(
    types.MINUS_QUANTITY_PRODUCT_REQUEST
  ),
  actMinusQuantityProductToCartSuccess: createAction(
    types.MINUS_QUANTITY_PRODUCT_SUCCESS
  ),

  actRemoveProductFromCart: createAction(
    types.REMOVE_PRODUCT_FROM_CART_REQUEST
  ),
  actRemoveProductFromCartSuccess: createAction(
    types.REMOVE_PRODUCT_FROM_CART_SUCCESS
  ),
}

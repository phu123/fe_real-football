import { takeLatest, all, put } from 'redux-saga/effects'
import { types, actionCreator } from './cart.meta'
import { notification } from 'antd'

function* sagaAddProductToCartMonitor(action) {
  try {
    yield put(actionCreator.actAddProductToCartSuccess(action.payload))
    notification.success({
      message: 'Thêm vào giỏ hàng thành công',
    })
  } catch (error) {
    console.log(error)
    notification.error({
      message: 'Thêm vào giỏ hàng không thành công',
    })
  }
}

function* sagaPlusQuantityProductToCartMonitor(action) {
  try {
    yield put(actionCreator.actPlusQuantityProductToCartSuccess(action.payload))
  } catch (error) {
    console.log(error)
  }
}

function* sagaMinusQuantityProductToCartMonitor(action) {
  try {
    yield put(
      actionCreator.actMinusQuantityProductToCartSuccess(action.payload)
    )
  } catch (error) {
    console.log(error)
  }
}

function* sagaRemoveProductFromCartMonitor(action) {
  try {
    yield put(actionCreator.actRemoveProductFromCartSuccess(action.payload))
  } catch (error) {
    console.log(error)
  }
}

export default function* cartSaga() {
  yield all([
    takeLatest(types.ADD_TO_CART_REQUEST, sagaAddProductToCartMonitor),

    takeLatest(
      types.PLUS_QUANTITY_PRODUCT_REQUEST,
      sagaPlusQuantityProductToCartMonitor
    ),

    takeLatest(
      types.MINUS_QUANTITY_PRODUCT_REQUEST,
      sagaMinusQuantityProductToCartMonitor
    ),

    takeLatest(
      types.REMOVE_PRODUCT_FROM_CART_REQUEST,
      sagaRemoveProductFromCartMonitor
    ),
  ])
}

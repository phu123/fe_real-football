import { handleActions } from 'redux-actions'

import { types } from './cart.meta'

const initialState = {
  items: [],
  total: 0,
}

const addToCartSuccess = (state, action) => {
  const { product, quantity } = action.payload
  const newProduct = {
    product,
    quantity,
  }
  const itemClone = [...state.items]
  const index = itemClone.findIndex(
    (x) => x.product.id === newProduct.product.id
  )
  if (index === -1) {
    itemClone.push(newProduct)
  } else {
    itemClone[index].quantity += newProduct.quantity
  }
  return {
    ...state,
    items: itemClone,
    total: itemClone.reduce((prev, curr) => prev + curr.quantity, 0),
  }
}

const plusQuantityProductToCartSuccess = (state, action) => {
  const id = action.payload
  const itemClone = [...state.items]
  const index = itemClone.findIndex((x) => x.product.id === id)
  if (index !== -1) {
    itemClone[index].quantity += 1
  }
  return {
    ...state,
    items: itemClone,
    total: itemClone.reduce((prev, curr) => prev + curr.quantity, 0),
  }
}

const minusQuantityProductToCartSuccess = (state, action) => {
  const id = action.payload
  const itemClone = [...state.items]
  const index = itemClone.findIndex((x) => x.product.id === id)
  if (index !== -1) {
    if (itemClone[index].quantity <= 1) {
      return
    } else {
      itemClone[index].quantity -= 1
    }
  }
  return {
    ...state,
    items: itemClone,
    total: itemClone.reduce((prev, curr) => prev + curr.quantity, 0),
  }
}

const removeProductFromCartSuccess = (state, action) => {
  const id = action.payload
  const itemClone = [...state.items]
  const index = itemClone.findIndex((x) => x.product.id === id)
  if (index !== -1) {
    itemClone.splice(index, 1)
  }
  return {
    ...state,
    items: itemClone,
    total: itemClone.reduce((prev, curr) => prev + curr.quantity, 0),
  }
}

export default handleActions(
  {
    [types.ADD_TO_CART_SUCCESS]: addToCartSuccess,
    [types.PLUS_QUANTITY_PRODUCT_REQUEST]: plusQuantityProductToCartSuccess,
    [types.MINUS_QUANTITY_PRODUCT_SUCCESS]: minusQuantityProductToCartSuccess,
    [types.REMOVE_PRODUCT_FROM_CART_SUCCESS]: removeProductFromCartSuccess,
  },
  initialState
)

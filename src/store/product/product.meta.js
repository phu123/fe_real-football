import { createAction } from 'redux-actions'

export const types = {
  GET_PRODUCTS_REQUEST: 'GET_PRODUCTS_REQUEST',
  GET_PRODUCTS_SUCCESS: 'GET_PRODUCTS_SUCCESS',

  GET_PRODUCT_BY_ID_REQUEST: 'GET_PRODUCT_BY_ID_REQUEST',
  GET_PRODUCT_BY_ID_SUCCESS: 'GET_PRODUCT_BY_ID_SUCCESS',
}

export const actionCreator = {
  getProducts: createAction(types.GET_PRODUCTS_REQUEST),
  getProductsSuccess: createAction(types.GET_PRODUCTS_SUCCESS),

  getProductById: createAction(types.GET_PRODUCT_BY_ID_REQUEST),
  getProductByIdSuccess: createAction(types.GET_PRODUCT_BY_ID_SUCCESS),
}

import { takeLatest, call, put, all } from 'redux-saga/effects'
import { notification } from 'antd'
import { getProducts, getProductById } from 'utils/api/apiRouter'

import { types, actionCreator } from './product.meta'

const errMessage = {
  message: 'Opp! Đã xảy ra lỗi',
  description: 'Liên hệ ban quản trị viên để biết thêm thông tin chi tiết!',
}

function* getProductMonitor(action) {
  try {
    const { body } = yield call(getProducts, action.payload)
    yield put(actionCreator.getProductsSuccess(body))
  } catch (error) {
    notification.error(errMessage)
  }
}

function* getProductByIdMonitor(action) {
  try {
    const { body } = yield call(getProductById, action.payload)
    yield put(actionCreator.getProductByIdSuccess(body))
  } catch (error) {
    notification.error(errMessage)
  }
}

export default function* productSaga() {
  yield all([
    takeLatest(types.GET_PRODUCTS_REQUEST, getProductMonitor),
    takeLatest(types.GET_PRODUCT_BY_ID_REQUEST, getProductByIdMonitor),
  ])
}

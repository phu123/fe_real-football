import { handleActions } from 'redux-actions'

import { types } from './product.meta'

const initialState = {
  items: {
    total: 0,
    list: [],
    byId: {},
  },
}

const getProductsSuccess = (state, action) => ({
  ...state,
  items: {
    ...state.items,
    bySlug: action.payload.data,
  },
})

const getProductByIdSuccess = (state, action) => ({
  ...state,
  items: {
    byId: action.payload,
  },
})

export default handleActions(
  {
    [types.GET_PRODUCTS_SUCCESS]: getProductsSuccess,
    [types.GET_PRODUCT_BY_ID_SUCCESS]: getProductByIdSuccess,
  },
  initialState
)

import React, { Suspense } from 'react'
import { Layout, Spin } from 'antd'
import 'antd/dist/antd.css'

import useMobile from './components/common/useMobile'
import Header from './components/common/Header'
import Footer from './components/common/Footer'
import MarqueeText from './components/common/MarqueeText'
import ErrorBoundary from './components/common/ErrorBoundary'
import Routes from './routes.js'

import './App.css'

const { Content } = Layout

function App() {
  const ismobile = useMobile()

  return (
    <Suspense
      fallback={
        <Spin
          size='large'
          style={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            height: '100vh',
            color: '#000000',
          }}
        />
      }
    >
      <Layout>
        <Header {...{ ismobile: ismobile.toString() }} />

        <Content
          style={{ marginTop: 64 }}
          {...{ ismobile: ismobile.toString() }}
        >
          <div style={{ backgroundColor: 'var(--primary)' }}>
            <MarqueeText
              style={{ marginTop: '64px' }}
              text='Chúng tôi cung cấp các loại giày bóng đá có chất lượng hàng đầu'
            />
          </div>
          <ErrorBoundary>
            <Routes />
          </ErrorBoundary>
        </Content>
        <Footer {...{ ismobile: ismobile.toString() }} />
      </Layout>
    </Suspense>
  )
}

export default App

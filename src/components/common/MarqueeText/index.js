import React from 'react'

import styles from './styles.module.css'

const MarqueeText = (props) => {
  const { text } = props
  return (
    <div className={styles.marquee}>
      <span>{text}</span>
    </div>
  )
}

export default MarqueeText

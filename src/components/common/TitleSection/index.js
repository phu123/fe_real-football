import React from 'react'
import { Typography } from 'antd'

const { Title } = Typography

const TitleSection = (props) => {
  const { title, level } = props
  return (
    <div style={{ width: '100%', display: 'flex', justifyContent: 'center' }}>
      <Title level={level}>{title}</Title>
    </div>
  )
}

export default TitleSection

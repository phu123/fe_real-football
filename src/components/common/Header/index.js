import React from 'react'
import { Col, Row, Layout, Typography } from 'antd'
import { Link } from '@reach/router'
import {
  UserOutlined,
  ShoppingCartOutlined,
  SearchOutlined,
} from '@ant-design/icons'

import styles from './style.module.css'

const { Header } = Layout
const { Title } = Typography

const HeaderClient = () => {
  return (
    <Header className={styles.headerWrapper}>
      <Row>
        <Col lg={{ offset: 0, span: 24 }}>
          <Row type='flex' align='middle'>
            <Col lg={{ offset: 0, span: 4 }}>
              <Link to='/' className={styles.headerTitle}>
                <Title level={3}>RealFootball</Title>
              </Link>
            </Col>

            <Col lg={{ offset: 0, span: 16 }}>
              <ul className={styles.headerNav}>
                <li>
                  <Link to='/'>Nike</Link>
                </li>
                <li>
                  <Link to='/'>Adidas</Link>
                </li>
                <li>
                  <Link to='/'>PUMA</Link>
                </li>
                <li>
                  <Link to='/'>New Balance</Link>
                </li>
                <li>
                  <Link to='/'>Mizuno</Link>
                </li>
                <li>
                  <Link to='/'>Under Armour</Link>
                </li>
              </ul>
            </Col>

            <Col
              lg={{ offset: 0, span: 4 }}
              className={styles.headerLinkContainer}
            >
              <div>
                <SearchOutlined />
              </div>
              <div>
                <UserOutlined />
              </div>
              <div>
                <ShoppingCartOutlined />
              </div>
            </Col>
          </Row>
        </Col>
      </Row>
    </Header>
  )
}

export default HeaderClient

import React from 'react'
import { Col, Row } from 'antd'
import { Link } from '@reach/router'
import { truncate } from 'lodash'
import sanitizeHtml from 'sanitize-html'

import styles from './style.module.css'

const BlogItem = (props) => {
  const { item } = props
  return (
    <Link style={{ display: 'block' }} to={`/bai-viet/${item?.slug}`}>
      <Row className={styles.blogItem} gutter={[0, 16]}>
        <Col md={{ offset: 0, span: 24 }} className={styles.blogImage}>
          <div
            className={styles.blogBackgroundImage}
            style={{ backgroundImage: `url(${item?.image})` }}
          />
        </Col>
        <Col className={styles.contentTitle} md={{ offset: 0, span: 24 }}>
          <h3 className={styles.title}>
            {truncate(sanitizeHtml(item?.title), {
              length: 150,
              separator: /,? +/,
            })}
          </h3>
          <p className={styles.content}>
            {truncate(sanitizeHtml(item?.content), {
              length: 150,
              separator: /,? +/,
            })}
          </p>
        </Col>
      </Row>
    </Link>
  )
}

export default BlogItem

import React from 'react'
import { Link } from '@reach/router'
import { Row, Col, Input, Layout } from 'antd'
import {
  FacebookOutlined,
  TwitterOutlined,
  InstagramOutlined,
  YoutubeOutlined,
} from '@ant-design/icons'

import styles from './style.module.css'

const { Footer } = Layout
const { Search } = Input

const FooterClient = () => {
  const socialsIcon = [
    {
      link: '/',
      icon: <FacebookOutlined />,
    },
    {
      link: '/',
      icon: <TwitterOutlined />,
    },
    {
      link: '/',
      icon: <InstagramOutlined />,
    },
    {
      link: '/',
      icon: <YoutubeOutlined />,
    },
  ]
  return (
    <Footer className={styles.footerWrapper}>
      <Row>
        <Col lg={{ offset: 2, span: 20 }}>
          <Row>
            <Col lg={{ offset: 0, span: 12 }} className={styles.footerContent}>
              <div className={styles.footerTitle}>How can we help?</div>
              <Row>
                <Col lg={{ offset: 0, span: 12 }}>
                  <div className={styles.footerLinkContainer}>
                    <Link to='/' className={styles.footerLink}>
                      Your delivery options
                    </Link>
                  </div>
                  <div className={styles.footerLinkContainer}>
                    <Link to='/' className={styles.footerLink}>
                      Your payment options
                    </Link>
                  </div>
                  <div className={styles.footerLinkContainer}>
                    <Link to='/' className={styles.footerLink}>
                      Returns
                    </Link>
                  </div>
                </Col>

                <Col lg={{ offset: 0, span: 12 }}>
                  <div className={styles.footerLinkContainer}>
                    <Link to='/' className={styles.footerLink}>
                      Log in or register
                    </Link>
                  </div>
                  <div className={styles.footerLinkContainer}>
                    <Link to='/' className={styles.footerLink}>
                      Why buy from us?
                    </Link>
                  </div>
                  <div className={styles.footerLinkContainer}>
                    <Link to='/' className={styles.footerLink}>
                      Get in touch
                    </Link>
                  </div>
                </Col>
              </Row>
            </Col>

            <Col lg={{ offset: 0, span: 6 }} className={styles.footerContent}>
              <Row>
                <div className={styles.footerTitle}>User information</div>
                <Col lg={{ offset: 0, span: 24 }}>
                  <div className={styles.footerLinkContainer}>
                    <Link to='/' className={styles.footerLink}>
                      Privacy
                    </Link>
                  </div>
                  <div className={styles.footerLinkContainer}>
                    <Link to='/' className={styles.footerLink}>
                      Sitemap
                    </Link>
                  </div>
                  <div className={styles.footerLinkContainer}>
                    <Link to='/' className={styles.footerLink}>
                      Affiliates
                    </Link>
                  </div>
                </Col>
              </Row>
            </Col>

            <Col lg={{ offset: 0, span: 6 }} className={styles.footerContent}>
              <Row>
                <div className={styles.footerTitle}>
                  Sign up to our mailing list
                </div>
                <Col lg={{ offset: 0, span: 24 }}>
                  <div>
                    <Search
                      className={styles.search}
                      placeholder='Email'
                      enterButton='SIGN UP'
                      size='large'
                      onSearch={(value) => console.log(value)}
                    />
                  </div>
                  <div className={styles.footerIcon}>
                    {socialsIcon.map((x, index) => (
                      <div key={index}>
                        <Link to={x.link} className={styles.footerIconLink}>
                          {x.icon}
                        </Link>
                      </div>
                    ))}
                  </div>
                </Col>
              </Row>
            </Col>
          </Row>
        </Col>
      </Row>
    </Footer>
  )
}

export default FooterClient

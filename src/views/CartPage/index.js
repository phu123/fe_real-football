import React, { useState } from 'react'
import { Row, Col, Button, Select, Empty, Tag } from 'antd'
import { useDispatch, useSelector } from 'react-redux'
import { Link } from '@reach/router'

import { calculateSalePrice } from 'utils/helper'
import { actionCreator } from 'store/cart/cart.meta'

import styles from './styles.module.css'

const CartPage = () => {
  const reduxAction = useDispatch()

  const currentCart = useSelector((store) => store.cart)
  const data = currentCart.items
  const delivery = 4.99
  const subTotal = data.reduce(
    (prev, curr) =>
      prev +
      curr.quantity *
        calculateSalePrice(curr.product.discount, curr.product.pricePre),
    0
  )
  const total = subTotal + delivery

  const [visible, setVisible] = useState(false)
  const [count, setCount] = useState(2)

  const { Option } = Select

  const onVisibleEditItemBlock = () => {
    const newCount = count + 1
    setCount(newCount)
    if (count % 2 !== 0) {
      setVisible(false)
    } else {
      setVisible(true)
    }
  }

  const BasketMain = () => (
    <Row justify='space-between' style={{ padding: '15px 0' }}>
      <Col lg={{ span: 13 }}>
        <Row>
          <Col lg={{ span: 24 }}>
            <h1 className={styles.basketTitle}>
              <strong>Giỏ hàng của bạn:</strong>
              <span className={styles.noOfItems}>
                {' '}
                {currentCart?.total}&nbsp;sản phẩm
              </span>
            </h1>
          </Col>
        </Row>
        {data.map((item, index) => (
          <Row className={styles.basketItem} key={index}>
            <Col lg={{ span: 24 }}>
              <Row>
                <Col lg={{ span: 6 }}>
                  <div className={styles.basketImage}>
                    <Tag className={styles.basketImageTag}>{`${
                      item?.product.discount * 100
                    }%`}</Tag>
                    <Link to={`/san-pham/${item.product.slug}`}>
                      <img
                        src={item?.product?.images[0]?.imgSrc}
                        alt={item?.product?.name}
                        border='0'
                      />
                    </Link>
                  </div>
                </Col>
                <Col lg={{ span: 18 }} className={styles.basketData}>
                  <div className={styles.basketItemTitleAndPrice}>
                    <div>
                      <Link
                        to={`/san-pham/${item?.product.slug}`}
                        className={styles.basketItemTitle}
                      >
                        {item?.product.name}
                      </Link>
                      <div className={styles.basketStockWarning}>
                        Last few selling fast - order quickly!
                      </div>
                    </div>
                    <div className={styles.basketPrice}>
                      VNĐ&nbsp;{item?.product.pricePre}
                    </div>
                  </div>

                  <div className={styles.basketColour}>
                    <strong>Colour: </strong>
                    UltraYellow/Blk
                  </div>

                  <div className={styles.basketSize}>
                    <strong>Size: </strong>36 UK
                  </div>

                  <div className={styles.basketQuantity}>
                    <strong>QTY: </strong>
                    {item?.quantity}
                  </div>

                  <div className={styles.basketButtonContainer}>
                    <Button
                      type='default'
                      size='large'
                      className={styles.basketButton}
                      onClick={onVisibleEditItemBlock}
                    >
                      Chỉnh sửa sản phẩm
                    </Button>
                    <Button
                      type='default'
                      size='large'
                      className={styles.basketButton}
                      onClick={() => {
                        reduxAction(
                          actionCreator.actRemoveProductFromCart(
                            item?.product.id
                          )
                        )
                      }}
                    >
                      Xóa khỏi giỏ hàng
                    </Button>
                  </div>

                  <div
                    className={styles.basketEditItemBlock}
                    style={
                      visible ? { display: 'block' } : { visibility: 'none' }
                    }
                  >
                    <div className={styles.productOptions}>
                      <Select
                        defaultValue='36'
                        size='large'
                        className={styles.productSize}
                      >
                        {item?.product.sizes.map((itemSize, indexItemSize) => (
                          <Option
                            value={itemSize?.sizeNumber}
                            key={indexItemSize}
                          >
                            {itemSize?.sizeNumber}
                          </Option>
                        ))}
                      </Select>
                      <Select
                        defaultValue='Qty 1'
                        size='large'
                        className={styles.productQuantity}
                      >
                        <Option value='1'>1</Option>
                        <Option value='2'>2</Option>
                        <Option value='3'>3</Option>
                        <Option value='4'>4</Option>
                      </Select>
                    </div>
                    <div className={styles.personalization}>
                      <h4 className={styles.personalizationTitle}>
                        Personalize your item
                      </h4>
                      <Select
                        defaultValue='none'
                        size='large'
                        className={styles.personalizationControl}
                      >
                        <Option value='Black'>Black</Option>
                        <Option value='Blue'>Blue</Option>
                        <Option value='Gold'>Gold</Option>
                        <Option value='Red'>Red</Option>
                      </Select>
                    </div>
                    <div className={styles.updateItemButton}>
                      <Button
                        type='default'
                        size='large'
                        className={styles.cancelButton}
                      >
                        Cancel
                      </Button>
                      <Button
                        type='default'
                        size='large'
                        className={styles.updateButton}
                      >
                        Update
                      </Button>
                    </div>
                  </div>
                </Col>
              </Row>
            </Col>
          </Row>
        ))}
      </Col>
      <Col lg={{ span: 10 }} style={{ backgroundColor: '#fafafa' }}>
        <Row className={styles.summaryContainer}>
          <Col lg={{ span: 24 }}>
            <h1 className={styles.summaryTitle}>
              <strong>Đơn đặt hàng</strong>
            </h1>
          </Col>
        </Row>
        <Row>
          <Col lg={{ span: 24 }} className={styles.basketSummary}>
            <div className={styles.basketSubTotal}>
              <div>Tạm tính: </div>
              <div>VNĐ&nbsp;{subTotal}</div>
            </div>
            <div className={styles.basketCarriage}>
              <div>Phí ship: </div>
              <div>VNĐ&nbsp;{delivery}</div>
            </div>
            <div className={styles.basketTotal}>
              <div>Tổng tiền: </div>
              <div>VNĐ&nbsp;{total}</div>
            </div>
            <Button
              type='default'
              size='large'
              className={styles.basketCheckout}
            >
              Pay securely now
            </Button>
            <div className={styles.paymentGraphic}>
              <Link to='/trang-chu'>
                <img
                  alt='Pay securely now'
                  src='https://www.lovellsoccer.co.uk/responsive-images/PaymentLogos/Group-Card-Logo-Images.png'
                  className={styles.paymentLogos}
                />
              </Link>
            </div>
            <div className={styles.checkoutDivider}>
              <span>&nbsp;hoặc&nbsp;</span>
            </div>
            <div className={styles.basketCheckSelection}>
              <div className={styles.basketCheckoutPayPal}>
                <Link to='#'>
                  <img
                    alt='paypal'
                    src='https://www.lovellsoccer.co.uk/responsive-images/PaymentLogos/paypal-logo-black.png'
                  />
                </Link>
              </div>
              <div className={styles.basketCheckoutAmazonPay}>
                <Link to='#'>
                  <img
                    alt='amazonPay'
                    src='https://www.lovellsoccer.co.uk/responsive-images/PaymentLogos/amazon-pay-logo-black.png'
                  />
                </Link>
              </div>
            </div>
          </Col>
        </Row>
      </Col>
    </Row>
  )

  const BasketEmpty = () => (
    <Row
      type='flex'
      justify='center'
      align='middle'
      className={styles.basketEmpty}
    >
      <Col span={24}>
        <Empty
          description={
            <span>
              Giỏ hàng của bạn đang trống. Chuyển đến trang{' '}
              <Link to='/san-pham/slug'>chi tiết sản phẩm</Link>
            </span>
          }
        />
      </Col>
    </Row>
  )

  return (
    <Row>
      <Col lg={{ offset: 2, span: 20 }}>
        {currentCart?.total === 0 ? <BasketEmpty /> : <BasketMain />}
      </Col>
    </Row>
  )
}

export default CartPage

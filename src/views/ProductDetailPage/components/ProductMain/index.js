import React, { useState } from 'react'
import { Row, Col, Radio, Button, Tag } from 'antd'
import { CheckCircleFilled } from '@ant-design/icons'

import { calculateSalePrice } from 'utils/helper'

import styles from './styles.module.css'

const ProductMain = (props) => {
  const { data } = props
  let images = data?.image
  const initImage = data?.image[0]
  const initMessageColor = '#19a85e'

  const [imageSrcZoom, setImageSrcZoom] = useState(initImage)
  const [backgroundPosition, setBackgroundPosition] = useState('0% 0%')
  const [count, setCount] = useState(1)
  const messageColor = initMessageColor
  const [sizeNumber, setSizeNumber] = useState(null)
  const [sizeActive, setSizeActive] = useState(false)

  const handleChangImageThumb = (newImgSrcZoom) => {
    setImageSrcZoom(newImgSrcZoom)
  }

  const handleMouseMove = (e) => {
    const { left, top, width, height } = e.target.getBoundingClientRect()
    const x = ((e.pageX - left) / width) * 100
    const y = ((e.pageY - top) / height) * 100
    setBackgroundPosition(`${x}% ${y}%`)
  }

  const handleClickMinusButton = () => {
    if (count <= 1) {
      return
    }
    setCount(count - 1)
    props.onMinusProductToCart(data.id)
  }

  const handleClickPlusButton = () => {
    const newCount = count + 1
    setCount(newCount)
    props.onPlusProductToCart(data.id)
  }

  const handleAddToCart = (e) => {
    e.preventDefault()
    props.onAddToCart({
      product: data,
      quantity: count,
    })
  }

  const onChangeSize = (e) => {
    setSizeNumber(e.target.value)
    setSizeActive(true)
  }

  const handleRenderByStatus = (status) => {
    let result = {
      message: '',
      icon: <></>,
    }
    switch (status) {
      case 'active':
        result = {
          message: 'Đang còn hàng',
          icon: <CheckCircleFilled />,
        }
        break
      default:
        break
    }
    return result
  }

  const ImageMain = () => (
    <Row justify='space-around'>
      <Col md={{ span: 3 }}>
        {images?.map((item, index) => (
          <Row align='middle' key={index}>
            <Col md={{ span: 24 }}>
              <div className={styles.thumbImageItem}>
                <img
                  className={styles.thumbImage}
                  alt={data?.name}
                  src={item}
                  onClick={() => handleChangImageThumb(item)}
                />
              </div>
            </Col>
          </Row>
        ))}
      </Col>
      <Col md={{ span: 20 }}>
        <Row align='middle'>
          <div className={styles.imageZoomContainer}>
            {data?.discount && (
              <Tag className={styles.imageZoomTag}>
                {data?.discount * 100 + '%'}
              </Tag>
            )}
            <figure
              onMouseMove={handleMouseMove}
              style={{
                backgroundPosition: `${backgroundPosition}`,
                backgroundImage: `url(${imageSrcZoom})`,
              }}
            >
              <img
                className={styles.imageZoomed}
                src={imageSrcZoom}
                alt='images'
              />
            </figure>
          </div>
        </Row>
      </Col>
    </Row>
  )

  const SideBar = () => (
    <div className={styles.sideBarContainer}>
      <Row>
        <Col md={{ span: 24 }}>
          <Row>
            <Col md={{ span: 24 }}>
              <h1 className={styles.productName}>{data?.name}</h1>
            </Col>
          </Row>
          <Row justify='start'>
            <Col md={{ span: 24 }}>
              <div className={styles.productCost}>
                <div className={styles.pricePre}>VNĐ&nbsp;{data?.price}</div>
                <span className={styles.priceMainSpecial}>
                  VNĐ&nbsp;{calculateSalePrice(data?.discount, data?.price)}
                </span>
              </div>
            </Col>
          </Row>
          <Row>
            <Col md={{ span: 24 }}>
              <div className={styles.sizeListContainer}>
                <Row>
                  <Col md={{ span: 24 }}>
                    <Radio.Group
                      buttonStyle='solid'
                      size='large'
                      defaultValue={sizeNumber}
                      onChange={onChangeSize}
                    >
                      <Row gutter={[8, 8]}>
                        {data?.sizes?.map((item, index) => (
                          <Col md={{ span: 6 }} key={index}>
                            <div className={styles.radioButtonContainer}>
                              <Radio.Button
                                type='primary'
                                value={item?.sizeNumber}
                                className={styles.btnSizeSelected}
                              >
                                {item?.sizeNumber}
                              </Radio.Button>
                            </div>
                          </Col>
                        ))}
                      </Row>
                    </Radio.Group>
                  </Col>
                </Row>
                <Row>
                  <Col md={{ span: 24 }}>
                    <div
                      className={styles.productMessage}
                      style={{ color: messageColor }}
                    >
                      <div
                        className={styles.iconMessage}
                        style={{ color: messageColor }}
                      >
                        {handleRenderByStatus(data?.status).icon}&nbsp;
                      </div>
                      {handleRenderByStatus(data?.status).message}
                    </div>
                  </Col>
                </Row>
                <Row>
                  <Col md={{ span: 24 }}>
                    <div className={styles.quantityCounter}>
                      <div
                        className={styles.minusButton}
                        onClick={handleClickMinusButton}
                      ></div>
                      <div className={styles.quantityWrapper}>
                        <span className={styles.label}>Số lượng</span>
                        <span className={styles.counter}>{count}</span>
                      </div>
                      <div
                        className={styles.plusButton}
                        onClick={handleClickPlusButton}
                      ></div>
                    </div>
                  </Col>
                </Row>
              </div>
            </Col>
          </Row>
          <Row>
            <Col md={{ span: 24 }}>
              <Button
                className={styles.addToCart}
                onClick={handleAddToCart}
                disabled={sizeActive ? false : true}
              >
                Thêm vào giỏ hàng
              </Button>
            </Col>
          </Row>
        </Col>
      </Row>
    </div>
  )

  return (
    <div className={styles.mainContent}>
      <Row gutter={[0, 24]}>
        <Col md={{ offset: 2, span: 20 }}>
          <Row justify='space-between'>
            <Col md={{ span: 14 }}>
              <ImageMain />
            </Col>
            <Col md={{ span: 9 }}>
              <SideBar />
            </Col>
          </Row>
        </Col>
      </Row>
    </div>
  )
}
export default ProductMain

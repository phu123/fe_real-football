import React, { useEffect } from 'react'
import { Row, Col, Collapse } from 'antd'
import { useDispatch, useSelector } from 'react-redux'
import sanitizeHtml from 'sanitize-html'

import { actionCreator } from 'store/cart/cart.meta'
import { actionCreator as productActionCreator } from 'store/product/product.meta'

import ProductMain from './components/ProductMain'
import styles from './styles.module.css'
import { useParams } from '@reach/router'

const { Panel } = Collapse

const ProductDetailPage = () => {
  const dispatch = useDispatch()

  const { byId: product } = useSelector((store) => store.product.items)
  const { id } = useParams()

  const getProductByID = (id) => {
    dispatch(productActionCreator.getProductById(id))
  }

  useEffect(() => {
    getProductByID(id)
    // eslint-disable-next-line
  }, [id])

  const handleAddProductToCart = ({ product, quantity }) => {
    dispatch(actionCreator.actAddProductToCart({ product, quantity }))
  }

  const handlePlusProductToCart = (productId) => {}

  const handleMinusProductToCart = (productId) => {}

  return (
    <div className={styles.productDataWrapper}>
      <Row align='middle'>
        <Col md={{ offset: 1, span: 22 }}>
          <Row>
            <Col md={{ span: 24 }}>
              <ProductMain
                data={product}
                onAddToCart={handleAddProductToCart}
                onPlusProductToCart={handlePlusProductToCart}
                onMinusProductToCart={handleMinusProductToCart}
              />
            </Col>
          </Row>
          <Row>
            <Col md={{ span: 24 }}>
              <Collapse>
                <Panel
                  header='Chi tiết về sản phẩm'
                  key='1'
                  style={{
                    fontWeight: 'bold',
                  }}
                >
                  <div>
                    <p className={styles.productDescription}>
                      {sanitizeHtml(product?.description)}
                    </p>
                  </div>
                </Panel>
                <Panel
                  header='Mã sản phẩm'
                  key='2'
                  style={{ fontWeight: 'bold' }}
                >
                  <div>
                    <span className={styles.productSpecsLabel}>
                      Mã code:&nbsp;
                    </span>
                    <span className={styles.productSpecsValue}>
                      {product?.id}
                    </span>
                  </div>
                  <div>
                    <span className={styles.productSpecsLabel}>
                      Thương hiệu:&nbsp;
                    </span>
                    <span className={styles.productSpecsValue}>
                      {product?.tradeMark}
                    </span>
                  </div>
                </Panel>
              </Collapse>
            </Col>
          </Row>
        </Col>
      </Row>
    </div>
  )
}

export default ProductDetailPage

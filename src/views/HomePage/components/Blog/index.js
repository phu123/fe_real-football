import React from 'react'
import { Row, Col } from 'antd'

import TitleSection from 'components/common/TitleSection'

import styles from './style.module.css'
import BlogItem from 'components/common/BlogItem'

const Blog = (props) => {
  return (
    <Col offset={1} span={22}>
      <div className={styles.blog}>
        <TitleSection title='Bài Viết' level={3} />
        <Row gutter={[16, 16]} style={{ marginTop: 12 }}>
          {props?.items?.list?.map((item, index) => (
            <Col md={8} key={index}>
              <BlogItem item={item} />
            </Col>
          ))}
        </Row>
      </div>
    </Col>
  )
}

export default Blog

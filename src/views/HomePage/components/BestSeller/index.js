import React from 'react'
import { Button, Row, Col } from 'antd'
import { LeftOutlined, RightOutlined } from '@ant-design/icons'
import Carousel, { consts } from 'react-elastic-carousel'
import { Link } from '@reach/router'

import TitleSection from 'components/common/TitleSection'

import styles from './style.module.css'

const BestSeller = () => {
  const renderPagination = ({ pages, activePage, onClick }) => <></>

  const renderArrow = ({ type, onClick }) => {
    const pointer = type === consts.PREV ? <LeftOutlined /> : <RightOutlined />
    return (
      <Button
        style={{
          border: 'none',
          padding: '0',
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'center',
          height: '100%',
          backgroundColor: '#ffffff',
        }}
        onClick={onClick}
      >
        {pointer}
      </Button>
    )
  }

  const BootItem = () => (
    <Link to='/'>
      <div
        className={styles.background}
        style={{
          background: `url('https://www.lovellsoccer.co.uk/products/399885.jpg')`,
        }}
      />

      <div>
        <Row className={styles.containItem} type='flex' gutter={8}>
          <Col md={{ offset: 0, span: 16 }} className={styles.titleName}>
            Nike Mercurial Vapor Elite Mens FG Football Boots
          </Col>
          <Col md={{ offset: 0, span: 8 }}>
            <div className={styles.titlePrice}>
              <div className={`${styles.price}`}>261.000 VNĐ</div>
              <div className={`${styles.price} ${styles.salePrice}`}>
                180.000 VNĐ
              </div>
            </div>
          </Col>
        </Row>
      </div>
    </Link>
  )
  return (
    <div style={{ marginTop: 40 }}>
      <TitleSection title='Most Wanted' level={3} />
      <Row>
        <Col lg={{ offset: 2, span: 20 }}>
          <Carousel
            className={styles.carousel}
            itemsToShow={4}
            showArrows={true}
            renderArrow={renderArrow}
            renderPagination={renderPagination}
            itemPadding={[0, 16]}
          >
            <BootItem />
            <BootItem />
            <BootItem />
            <BootItem />
            <BootItem />
          </Carousel>
        </Col>
      </Row>
    </div>
  )
}

export default BestSeller

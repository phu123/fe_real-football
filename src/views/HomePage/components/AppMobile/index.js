import React, { memo } from 'react'

import styles from './styles.module.css'

const AppMobile = () => (
  <div className={styles.backgroundImage}>
    <img src='/asset/images/app-desktop.jpg' alt='desktop app' />
  </div>
)

export default memo(AppMobile)

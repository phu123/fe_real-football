import React from 'react'
import { Link } from '@reach/router'
import { List } from 'antd'

import styles from './style.module.css'

const ListBrands = () => {
  const brands = [
    {
      id: 1,
      background:
        'https://www.lovellsoccer.co.uk/features/splash-pages/soccer/boots/nike-boots.jpg?v=5',
      link: '/giay-bong-da/nike',
      title: 'Nike Boots',
    },
    {
      id: 2,
      background:
        'https://www.lovellsoccer.co.uk/features/splash-pages/soccer/boots/adidas-boots.jpg?v=6',
      link: '/giay-bong-da/adidas',
      title: 'Adidas Boots',
    },
    {
      id: 3,
      background:
        'https://www.lovellsoccer.co.uk/features/splash-pages/soccer/boots/puma-boots.jpg?3',
      link: '/giay-bong-da/puma',
      title: 'PUMA Boots',
    },
    {
      id: 4,
      background:
        'https://www.lovellsoccer.co.uk/features/splash-pages/soccer/boots/new-balance-boots.jpg?4',
      link: '/giay-bong-da/new-balance',
      title: 'New Balance Boots',
    },
    {
      id: 5,
      background:
        'https://www.lovellsoccer.co.uk/features/splash-pages/soccer/boots/mizuno-boots.jpg?3',
      link: '/giay-bong-da/mizuno',
      title: 'Mizuno Boots',
    },
    {
      id: 6,
      background:
        'https://www.lovellsoccer.co.uk/features/splash-pages/soccer/boots/under-armour-boots.jpg?3',
      link: '/giay-bong-da/under-armour',
      title: 'Under Armour Boots',
    },
  ]

  const Brands = (item) => {
    return (
      <List.Item style={{ padding: '8px 16px' }}>
        <Link to={item.link}>
          <div className={styles.brandContainer}>
            <div className={styles.brandImage}>
              <img src={item.background} alt={item.title} />
            </div>
            <div className={styles.brandContent}>
              <div className={styles.brandTitle}>{item.title}</div>
              <div className={styles.brandShopnow}>Shop now</div>
            </div>
          </div>
        </Link>
      </List.Item>
    )
  }

  return (
    <div style={{ backgroundColor: `var(--secondary)` }}>
      <List
        grid={{
          xs: 1,
          sm: 1,
          md: 2,
          lg: 3,
          xl: 3,
          xxl: 3,
        }}
        size='large'
        rowKey='id'
        dataSource={brands}
        renderItem={Brands}
      />
    </div>
  )
}

export default ListBrands

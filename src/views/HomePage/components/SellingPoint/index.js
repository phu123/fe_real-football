import React from 'react'
import { Space } from 'antd'

import styles from './style.module.css'

const SellingPoint = () => {
  const sellingPoints = [
    {
      imgSrc:
        'https://www.lovellsoccer.co.uk/responsive-images/LS-CTA-delivery.svg',
      content: ' ORDER BEFORE 1PM FOR NEXT DAY DELIVERY',
    },
    {
      imgSrc:
        'https://www.lovellsoccer.co.uk/responsive-images/LS-CTA-money-back.svg',
      content: '28 DAY MONEY BACK GUARANTEE',
    },
    {
      imgSrc:
        'https://www.lovellsoccer.co.uk/responsive-images/LS-CTA-call-centre.svg',
      content: 'CALL CENTRE OPEN MON-FRI 9AM-5PM',
    },
  ]

  return (
    <div className={styles.homeSellingPoints}>
      <Space size={24} align='center'>
        {sellingPoints.map((item, index) => (
          <div className={styles.homeSellingPoint} key={index}>
            <img src={item.imgSrc} alt={item.content} />
            {item.content}
          </div>
        ))}
      </Space>
    </div>
  )
}
export default SellingPoint

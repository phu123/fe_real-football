import React, { memo } from 'react'
import { Carousel } from 'antd'

import styles from './style.module.css'

const Slider = () => {
  const srcImages = [
    { src: '/asset/images/30-Off-Tormentor-Desktop.jpg' },
    { src: '/asset/images/Ultraboost-Sale-Desktop.jpg' },
    { src: '/asset/images/40-off-Mutator-Desktop.jpg' },
  ]

  return (
    <>
      <Carousel className={styles.customSlider} autoplay>
        {srcImages.map((srcItem, index) => (
          <div key={index}>
            <img src={srcItem.src} alt={srcItem.src} />
          </div>
        ))}
      </Carousel>
    </>
  )
}
export default memo(Slider)

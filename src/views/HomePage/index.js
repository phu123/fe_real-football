import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'

import { actionCreator } from 'store/blog/blog.meta'

import SellingPoint from './components/SellingPoint'
import ListBrands from './components/ListBrands'
import BestSeller from './components/BestSeller'
import Slider from './components/Slider'
import AppMobile from './components/AppMobile'
import Blog from './components/Blog'

const LIMIT = 3

const HomePage = (props) => {
  const dispatch = useDispatch()
  const { items = {} } = useSelector((store) => store.blog)
  const getBlogs = () => {
    dispatch(actionCreator.getBlogs({ limit: LIMIT }))
  }

  useEffect(() => {
    getBlogs()
    // eslint-disable-next-line
  }, [LIMIT])

  return (
    <div>
      <Slider />
      <SellingPoint />
      <ListBrands />
      <BestSeller />
      <Blog items={items} />
      <AppMobile />
    </div>
  )
}

export default HomePage

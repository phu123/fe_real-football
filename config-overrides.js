const { override, fixBabelImports, addLessLoader } = require('customize-cra')
const aliyunTheme = require('@ant-design/aliyun-theme')

module.exports = override(
  fixBabelImports('antd', {
    libraryDirectory: 'es',
    style: true,
  }),
  addLessLoader({
    javascriptEnabled: true,
    modifyVars: {
      ...aliyunTheme,
      '@primary-color': '#000000',
      '@text-color': '#000000',
      '@link-color': '#000000',
    },
  })
)
